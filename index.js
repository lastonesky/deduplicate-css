let postcss = require('postcss')

const path = require('path')
const Koa = require('koa')
const Router = require('koa-router')
const bodyParser = require('koa-bodyparser')
const app = new Koa()
const router = new Router()
const static = require('koa-static')
const dedupe = require('./dedupe.js')
// 静态资源目录对于相对入口文件index.js的路径
const staticPath = './static'

app.use(static(
    path.join( __dirname,  staticPath)
))

// app.use(async (ctx, next) => {
//     console.log(`${ctx.method}`,`${ctx.url}`);
//     await next()
//     console.log(`1`);
// })

router.get('/',async (ctx)=>{
    ctx.type = 'text/html'
    ctx.body = 'Hello KOA'
})
router.post('/css',async  (ctx)=>{
    const rb = ctx.request.body
    let ruleA = [],atRuleA = [],ruleB = [],atRuleB = []
    let cssA = postcss.parse(rb['a'])
    let cssB = postcss.parse(rb['b'])
    dedupe.dedupe(cssA,cssB)
    ctx.response.body = cssB.toString()
})

app.use(bodyParser({
    enableTypes: ['json','form'],
    formLimit: '1mb',
    jsonLimit: '2mb'
}))
app.use(router.routes())
app.listen(3000);