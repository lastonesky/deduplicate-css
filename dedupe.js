'use strict';

exports.__esModule = true;

var _postcss = require('postcss');

function noop() {}

function trimValue(value) {
    return value ? value.trim() : value;
}

function empty(node) {
    return !node.nodes.filter(function (child) {
        return child.type !== 'comment';
    }).length;
}

function equals(a, b) {
    if (a.type !== b.type) {
        return false;
    }

    if (a.important !== b.important) {
        return false;
    }

    if (a.raws && !b.raws || !a.raws && b.raws) {
        return false;
    }

    switch (a.type) {
        case 'rule':
            if (a.selector !== b.selector) {
                return false;
            }
            break;
        case 'atrule':
            if (a.name !== b.name || a.params.replace(/ /g,'') !== b.params.replace(/ /g,'')) {
                return false;
            }

            if (a.raws && trimValue(a.raws.before) !== trimValue(b.raws.before)) {
                return false;
            }

            if (a.raws && trimValue(a.raws.afterName) !== trimValue(b.raws.afterName)) {
                return false;
            }
            break;
        case 'decl':
            if (a.prop !== b.prop || a.value !== b.value) {
                return false;
            }

            if (a.raws && trimValue(a.raws.before) !== trimValue(b.raws.before)) {
                return false;
            }
            break;
    }

    if (a.nodes) {
        if (a.nodes.length !== b.nodes.length) {
            return false;
        }

        for (var i = 0; i < a.nodes.length; i++) {
            if (!equals(a.nodes[i], b.nodes[i])) {
                return false;
            }
        }
    }
    return true;
}

function dedupeRule(last, nodes) {
    let index = nodes.length - 1;

    let _loop = function _loop() {
        let node = nodes[index--];
        if (node && node.type === 'rule' && node.selector === last.selector) {
            last.each(function (child) {
                if (child.type === 'decl') {
                    dedupeNode(child, node.nodes);
                }
            });

            if (empty(last)) {
                last.remove();
            }
        }
    };

    while (index >= 0) {
        _loop();
    }
}

function dedupeNode(last, nodes) {
    let index = nodes.length - 1;

    while (index >= 0) {
        let _node = nodes[index--];
        if (_node && equals(_node, last)) {
            last.remove();
        }
    }
}

let handlers = {
    rule: dedupeRule,
    atrule: dedupeNode,
    decl: dedupeNode,
    comment: noop
};

function dedupe(rootA,rootB) {
    let aNodes = rootA.nodes;
    let bNodes = rootB.nodes;
    if (!bNodes) {
        return;
    }

    let index = bNodes.length - 1;
    while (index >= 0) {
        let last = bNodes[index--];
        if (!last || !last.parent) {
            continue;
        }
        dedupe(rootA,last);
        handlers[last.type](last, aNodes);
    }
}
module.exports = {
    dedupe : dedupe
}
// exports.default = (0, _postcss.plugin)('postcss-discard-duplicates', function () {
//     return dedupe;
// });
// module.exports = exports['default'];